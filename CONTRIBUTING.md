# Pandora

Pandora is made with :heart: by [USPCodeLab](https://github.com/uspcodelab)
for the Academic Center (CAMat) of the Institute of Mathematics and Statistics
of the University of São Paulo (IME-USP).

## Requirements

This project uses [Docker](https://docker.com) for development,
test and deployment.

Install Docker by following the instructions in one of the following links,
depending on your OS:
- Mac: https://docs.docker.com/docker-for-mac/install/
- Windows: https://docs.docker.com/docker-for-windows/install/
- Ubuntu: https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/
- Debian: https://docs.docker.com/engine/installation/linux/docker-ce/debian/
- Arch Linux: https://wiki.archlinux.org/index.php/Docker

To make the development easier, the project also uses [Docker Compose][3]
to orchestrate multiple containers together.

Install Docker Compose by following the instructions in
https://docs.docker.com/compose/install/.

## Setup

- To set up the PWA server for development, run:
  ```
  docker-compose build
  docker-compose up
  ```
  **NOTE:** These commands will **create a Docker image for the PWA**,
  and **load the server**. By default, the PWA will be available at
  `localhost:8080`.

## Observations

- The `build` operation may take a while depending on your internet connetion.
  It will download all dependencies specified at `package.json` and `yarn.lock`
  whenever these files are changed.

- The PWA assumes that the API is running at `localhost:3000`.

## Known issues

- Compose emits a warning in case you run it for the API first:
  ```
  WARNING: Found orphan containers (pandora_api_1, pandora_db_1, pandora_cache_1) for this project. If you removed or renamed this service in your compose file, you can run this command with the --remove-orphans flag to clean it up.
  ```
  This is the expected behavior, given that containers in the same
  namespace (`pandora`) but not listed in `docker-compose.yml`
  may be orphans.

- A directory `node_modules` may be created empty with root ownership (Linux).
  This is the expected behavior, given that packages downloaded by NPM are
  stored in a volume.

- A directory `ts-node` may be created with root ownership (Linux).
  This directory can be safely ignored and is not loaded by Git or Docker.

