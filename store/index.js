import Vuex from 'vuex'
import user from "./modules/user";
import signup from "./modules/signup";
import login from "./modules/login";
import reserve from "./modules/reserve";
import history from "./modules/history";

const createStore = () => {
  return new Vuex.Store({
    modules: {
      user,
      signup,
      login,
      reserve,
      history
    }
  });
}

export default createStore;
