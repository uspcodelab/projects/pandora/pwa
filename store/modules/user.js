const state = {
  user: {
    token: null,
    email: null
  }
};

const getters = {
  loggedEmail: state => {
    return state.user.email;
  },
  authToken: state => {
    return state.user.token;
  }, 
  isUserLogged: state => {
    return state.user.token !== null;
  }
};

const mutations = {
  setToken: (state, token) => {
    state.user.token = token;
  },
  setUserEmail: (state, email) => {
    state.user.email = email;
  },
  logout: state => {
    state.user.token = null;
    state.user.email = null;
  }
};

export default {
  state,
  getters,
  mutations
};
