const state = {
  signup: {
    name: '',
    nusp: '',
    email: '',
    password: '',
    errors: {}
  }
};

const getters = {
  signupInfo: state => {
    return state.signup;
  },
  signupErrors: state => {
    return state.signup.errors;
  }
};

const mutations = {
  setSignupError: (state, error) => {
    state.signup.errors[error.type] = error.value;
  },
  setSignupName: (state, newName) => {
    state.signup.name = newName;
  },
  setSignupNusp: (state, newNusp) => {
    state.signup.nusp = newNusp;
  },
  setSignupEmail: (state, newEmail) => {
    state.signup.email = newEmail;
  },
  setSignupPassword: (state, newPass) => {
    state.signup.password = newPass;
  },
  clearSignupInfo: state => {
    state.signup.name = '';
    state.signup.email = '';
    state.signup.nusp = '';
    state.signup.password = '';
  }
};

export default {
  state,
  getters,
  mutations
};
