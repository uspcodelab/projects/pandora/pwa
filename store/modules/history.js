const state = {
    history: {
      semesters: [null],
      status: [null]
    }
};
  
const getters = {
  historyData: state => {
    return state.history;
  },
  semesters: state => {
    return state.history.semesters;
  },
  status: state => {
    return state.reserve.status;
  }
};
  /*essa maneira de produzir os vetores de semestres e status ainda
  está muito ruim, e não corresponde com o que precisamos para iterar no
  v-for. */
const mutations = {
  setHistoryData: (state, payload) => {
    console.log('SSSs');
    for(i = 0; i < payload.length; i++){
      state.reserve.status = state.reserve.status.concat(payload[i].text);
      var updated_at = payload[i].updated_at;
      var month = updated_at.substring(5, 7);

      if (month < 7) //determina se é o primeiro ou segundo semestre
        var sAsArray = [updated_at.substring(0,3), 1];
      else
        var sAsArray = [updated_at.substring(0,3), 2];

      state.history.semesters = state.history.semesters.concat(sAsArray);
    }
  }
};
  
export default {
  state,
  getters,
  mutations
};