const state = {
  reserve: {
    acceptTerms: false,
    contractType: 'semiannual',
    places: null,
    positions: null,
    features: null,
    selectedPreferences: [null, null, null, null, null],
    allPreferencesAreSelected: false
  }
};

const getters = {
  reserveData: state => {
    return state.reserve;
  },
  acceptTerms: state => {
    return state.reserve.acceptTerms;
  },
  contractType: state => {
    return state.reserve.contractType;
  },
  places: state => {
    return state.reserve.places;
  },
  positions: state => {
    return state.reserve.positions;
  },
  features: state => {
    return state.reserve.features;
  },
  selectedPreferences: state => {
    return state.reserve.selectedPreferences;
  },
  enabledPreferences: state => {
    return state.reserve.enabledPreferences;
  },
  allPreferencesAreSelected: state => {
    return state.reserve.allPreferencesAreSelected;
  }
};

const mutations = {
  setReserveAcceptance: (state, value) => {
    state.reserve.acceptTerms = value;
  },
  setReserveContract: (state, value) => {
    state.reserve.contractType = value ? 'semiannual' : 'annual';
  },
  setReserveFeatures: (state, payload) => {
    state.reserve.places = payload.places;
    state.reserve.positions = payload.positions;
    state.reserve.features = payload.features;
  },
  addReservePreference: (state, payload) => {

    state.reserve.selectedPreferences[payload.feature] =
    {
      feature: payload.feature,
      place: payload.place,
      position: payload.position,
      priority: payload.priority,  
    };
    let pref = state.reserve.selectedPreferences;
    let areSelecteds = true;
    for(var i=1; i < pref.length; i++){
      if(pref[i] == null){
        areSelecteds = false;
        break;
      }
    }
    state.reserve.allPreferencesAreSelected = areSelecteds;
    console.log(state);
  },
  clearReserveInfo: (state, payload) => {
    state.reserve.acceptTerms = false;
    state.reserve.contractType = 'semiannual';
    state.reserve.selectedPreferences = [null, null, null, null, null];
    state.reserve.allPreferencesAreSelected = false;
  }
};

export default {
  state,
  getters,
  mutations
};
