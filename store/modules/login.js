const state = {
  login: {
    email: '',
    password: ''
  }
};

const getters = {
  loginInfo: state => {
    return state.login;
  }
};

const mutations = {
  clearLoginInfo: state => {
    state.login.email = '';
    state.login.password = '';
  },
  setLoginEmail: (state, email) => {
    state.login.email = email;
  },
  setLoginPassword: (state, password) => {
    state.login.password = password;
  }
};

export default {
  state,
  getters,
  mutations
};
