################################################################################
#                                  BASE IMAGE                                  #
################################################################################

# Use official Node's alpine image as base
FROM node:9.3-alpine AS base

# Create PWA_PATH if it doesn't exist and set it as the working dir
ENV PWA_PATH=/usr/src/pwa
WORKDIR $PWA_PATH

# Define base environment variables for the PWA
ENV HOST=0.0.0.0 \
    PORT=8080

# Expose default port to connect with the service
EXPOSE $PORT

################################################################################
#                               DEVELOPMENT IMAGE                              #
################################################################################

# Use base image to create the development image
FROM base AS development

# Set development environment development
ENV NODE_ENV=development

# Copy package.json and package-lock.json
COPY package.json yarn.lock ./

# Install dependencies and create a volume for them
RUN yarn install

# Copy the application code to the build path
COPY . .

# Define default command to execute when running the container
CMD [ "yarn", "run", "dev" ]

################################################################################
#                                 BUILDER IMAGE                                #
################################################################################

# Use base image to create the builder image
FROM base AS builder

# Copy code from the development container
COPY --from=development $PWA_PATH ./

# Transpile, minify, uglify and bundle code
RUN yarn run build

# Remove all files but the ones listed below (necessary for production)
RUN find * -maxdepth 0 \( \
      -name 'dist' -o \
      -name 'server' -o \
      -name 'node_modules' -o \
      -name 'nuxt.config.js' -o \
      -name 'package.json' -o \
      -name 'yarn.lock' -o \
    \) -prune -o -exec rm -rf '{}' ';'

################################################################################
#                               PRODUCTION IMAGE                               #
################################################################################

# Use base image to create the production image
FROM base AS production

# Set production environment variables
ENV NODE_ENV=production

# Copy code from the builder container
COPY --from=builder $PWA_PATH ./

# Remove development dependencies
RUN yarn install --frozen-lockfile --ignore-scripts --prefer-offline --force

# Define default command to execute when running the container
CMD [ "yarn", "run", "start" ]
