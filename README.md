# Pandora System (PWA)

PWA for the Pandora locker management system.

For further info, check the contribution guidelines at [CONTRIBUTING.md][1].

[1]: https://gitlab.com/eris-system/api/blob/master/CONTRIBUTING.md
